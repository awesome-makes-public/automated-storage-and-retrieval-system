# Automated storage and retrieval system

A system that automatically stores and retrieves small plastic bins. Made with 3D-printed parts, aluminum extrusions, stepper motors and Arduino.
More information can be found on https://hackaday.io/project/184394-automated-storage-and-retrieval-system
