#include <SpeedyStepper.h>
const int MOTOR_STEP_PIN = 2;
const int MOTOR_DIRECTION_PIN = 5;
const int MOTORY_STEP_PIN = 3;
const int MOTORY_DIRECTION_PIN = 6;

SpeedyStepper stepperX;
SpeedyStepper stepperY;

void setup(){
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  digitalWrite(8, LOW);

  stepperX.connectToPins(MOTOR_STEP_PIN, MOTOR_DIRECTION_PIN);
  stepperY.connectToPins(MOTORY_STEP_PIN, MOTORY_DIRECTION_PIN);
}

void loop() {
  stepperX.setStepsPerRevolution(200 * 16);
  stepperY.setStepsPerRevolution(200 * 16);

  stepperX.setSpeedInRevolutionsPerSecond(1.5);
  stepperX.setAccelerationInRevolutionsPerSecondPerSecond(0.5);

  stepperY.setSpeedInRevolutionsPerSecond(1);
  stepperY.setAccelerationInRevolutionsPerSecondPerSecond(0.5);

  stepperX.setupRelativeMoveInRevolutions(-5);
  stepperY.setupRelativeMoveInRevolutions(-5);
  
  while (!stepperX.motionComplete() || !stepperY.motionComplete()) {
    stepperX.processMovement();
    stepperY.processMovement();
  }
  delay(1000);

  stepperX.setupRelativeMoveInRevolutions(5);
  stepperY.setupRelativeMoveInRevolutions(5);
  while (!stepperX.motionComplete() || !stepperY.motionComplete()) {
    stepperX.processMovement();
    stepperY.processMovement();
  }
  delay(1000);
}
