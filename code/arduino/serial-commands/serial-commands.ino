// Program to test the AS-RS horizontal and vertical axis
// Uses serial input to move the steppers

#include <SpeedyStepper.h>
const int MOTOR_STEP_PIN = 2;
const int MOTOR_DIRECTION_PIN = 5;
const int MOTORY_STEP_PIN = 3;
const int MOTORY_DIRECTION_PIN = 6;

SpeedyStepper stepperX;
SpeedyStepper stepperY;

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete
void setup(){
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(11, INPUT_PULLUP);
  digitalWrite(8, LOW);

  stepperX.connectToPins(MOTOR_STEP_PIN, MOTOR_DIRECTION_PIN);
  stepperY.connectToPins(MOTORY_STEP_PIN, MOTORY_DIRECTION_PIN);

  inputString.reserve(200);

  stepperX.setStepsPerRevolution(200 * 16);
  stepperY.setStepsPerRevolution(200 * 16);

  stepperX.setSpeedInRevolutionsPerSecond(2);
  stepperX.setAccelerationInRevolutionsPerSecondPerSecond(1);

  stepperY.setSpeedInRevolutionsPerSecond(2);
  stepperY.setAccelerationInRevolutionsPerSecondPerSecond(1);
}

void loop() {
  if (stringComplete) {
    Serial.println(inputString.toFloat());

    stepperY.setupRelativeMoveInRevolutions(inputString.toFloat());
//    stepperX.setupRelativeMoveInRevolutions(inputString.toFloat());
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  while (!stepperX.motionComplete() || !stepperY.motionComplete() && digitalRead(11)) {
    stepperX.processMovement();
    stepperY.processMovement();
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
