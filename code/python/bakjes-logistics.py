# Importing Libraries
import serial
import time
upDown = serial.Serial(port='COM7', baudrate=115200, timeout=None)
platform = serial.Serial(port='COM17', baudrate=115200, timeout=None)
def beweegOmhoogOmlaag(x):
    upDown.write(bytes(x + "\n", 'utf-8'))
    time.sleep(0.05)
    data = upDown.readline()
    return data

def beweegPlatform(direction):
    platform.write(bytes(direction + "\n", 'utf-8'))
    time.sleep(0.05)
    data = platform.readline()
    return data

while True:
    input("druk op enter") # Taking input from user
    time.sleep(10)
    print("Naar links bewegen")
    returnedData = beweegPlatform("left")
    print(returnedData)
    time.sleep(0.2)

    print("0.5 omhoog")
    returnedData = beweegOmhoogOmlaag("0.3")
    print(returnedData)
    time.sleep(0.2)

    print("Naar home bewegen")
    returnedData = beweegPlatform("home")
    print(returnedData)
    time.sleep(0.2)

    print("2.3 omlaag")
    returnedData = beweegOmhoogOmlaag("-2.3")
    print(returnedData)
    time.sleep(0.2)

    print("Naar links")
    returnedData = beweegPlatform("left")
    print(returnedData)
    time.sleep(0.2)

    print("0.5 omlaag")
    returnedData = beweegOmhoogOmlaag("-0.3")
    time.sleep(0.2)

    print("Naar home bewegen")
    returnedData = beweegPlatform("home")
    print(returnedData)
    time.sleep(0.2)

    print("2.3 omhoog")
    returnedData = beweegOmhoogOmlaag("2.3")
    print(returnedData)
